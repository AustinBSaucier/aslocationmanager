//
//  ASAppDelegate.h
//  ASLocationManagerExample
//
//  Created by Austin Saucier on 9/11/13.
//  Copyright (c) 2013 Austin Saucier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end