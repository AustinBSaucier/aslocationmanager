//
//  ASLocationManagerExampleViewController.h
//  ASLocationManagerExample
//
//  Created by Austin Saucier on 19/11/13.
//  Copyright (c) 2013 Austin Saucier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASLocationManagerExampleViewController : UIViewController

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) UILabel *textLabel;

@end
