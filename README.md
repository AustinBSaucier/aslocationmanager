# ASLocationManager

A simple utility class for determining a user's current location on iOS. This class encapsulates CLLocationManager with an elegant, block-based Objective-C interface.

## Usage

### Add ASLocationManager to your project

1. Download or clone this repository.
2. Add both the header and implementation file from the `ASLocationManager` folder to your project.
3. Link your project with CoreLocation.framework.

### Example implementation

```objective-c
//  Get ASLocationManager singleton instance
ASLocationManager *locationManager = [FTLocationManager sharedManager];

//  Ask the location manager to grab the current location and get notified using the provided handler block
[locationManager updateLocationWithCompletionHandler:^(CLLocation *location, NSError *error, BOOL locationServicesDisabled) {
    if (error)
    {
        //  Handle any errors
        if (locationServicesDisabled) {
            //  Ask the user to enable location services...
        }
    }
    else
    {
        //  Do something cool with the user's current location
    }
}];
```

And you're done!

### Additional configuration

You can customize the behavior of ASLocationManager using properties, but you probably won't need to. The default values should satisfy most use cases.

#### maxErrorsCount
Automatically skip the first *N* errors.

Default setting is 3 errors (handler block with error will be called on 3rd error returned by internal CLLocationManager).

#### errorTimeout
The amount of time to wait before stopping the location request. 

Default setting is 3 seconds (if the internal CLLocationManager does not give any location in 3s from calling `updateLocationWithCompletionHandler:`, the handler block will be called with `ASLocationManagerErrorDomain` domain and `ASLocationManagerErrorCodeTimedOut` status code).

## Implementation details
Please check out the included sample project for details on implementation.

## Compatibility
Tested on iOS 5.0+ with ARC

## License
<pre>
The MIT License (MIT)

Copyright (c) 2013 Austin Saucier. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
</pre>
